package ua.nure.yaritenko.Practice6;


public class Tree<E extends Comparable<E>> {
	
	private Node<E> head;
	String s = new String("  ");
	
	public boolean addTo(Node<E> node, E value) {
		
		if (value.compareTo(node.value) < 0) {
			if (node.left == null) {
				node.left = new Node<>(value);
				node.left.up = node;
				node.left.spacesForPrint = s;
				
				s = "  ";
			} else {
				s += "  ";
				addTo(node.left, value);				
			}			
		}
		
		if (value.compareTo(node.value) > 0) {
			if (node.right == null) {
				node.right = new Node<>(value);
				node.right.up = node;
				node.right.spacesForPrint = s;
				
				s = "  ";
			} else {
				s += "  ";
				addTo(node.right, value);			
			}
		}
		
		if (node.value.compareTo(value) == 0) {
			return false;
		} else return true;
				
	}
	
	
	public boolean add(E element) {
		if (head == null) {
			head = new Node<>(element);
			head.spacesForPrint = "";
			
			return true;
		} else {
			return(addTo(head, element));
		}
	}
	
	public void add(E[] elements) {
		for (E el : elements) {
			add(el);
		}
	}
	
	public boolean remove(E element) {
		if (searchNode(head, element) != null)
			return removeTo(searchNode(head, element), element);
		else
			return false;
	}

	public void replace(Node<E> node) {
		if (node != null) {
			replace(node.left);
			replace(node.right);
			node.spacesForPrint = node.up.spacesForPrint;
		}
	}
	
	public boolean removeTo(Node<E> node, E element) {

		Node<E> parent = node.up;

		if (node.left == null && node.right == null) 
			node.value = null;

		if ((node.left == null || node.right == null) && !(node.left == null && node.right == null)) {
			if (node.left == null)
				if (parent.left == node) {
					replace(node.right);
					node.up.left = node.right;
				}
				else {
					replace(node.right);
					node.up.right = node.right;
				}
			if (node.right == null)
				if (parent.left == node) {
					replace(node.left);
					node.up.left = node.left;
				}
				else {
					replace(node.left);
					node.up.right = node.left;
				}
		}

		if (node.left != null && node.right != null) {
			Node<E> heir = nextNode(node);
			node.value = heir.value;
			heir.value = null;
		}

		return true;
		
	}
	
	public Node<E> searchNode(Node<E> node, E element) {
		if (node == null || element == node.value)
			return node;
		if (element.compareTo(node.value) < 0)
			return searchNode(node.left, element);
		else 
			return searchNode(node.right, element);
		
		
	}
	
	public Node<E> minNode(Node<E> node) {
		if (node.left == null)
			return node;
		return minNode(node.left);
	}
	
	public Node<E> nextNode(Node<E> node) {
		if (node.right != null)
			return minNode(node.right);
		Node<E> parent = node.up;
		while (parent != null && node == parent.right) {
			node = parent;
			parent = parent.up;
		}
		
		return parent;
			
	}
	
	public void printTo(Node<E> node) {
		if (node != null) {
			printTo(node.left);
			if (node.value == null) 
				System.out.println();
			else
				System.out.println(node.spacesForPrint + node.value);
			printTo(node.right);
		}

	}
	
	public void print() {
		printTo(head);
	}
	
	private static class Node<E> {
		
		public E value;
		public Node<E> left;
		public Node<E> right;
		public Node<E> up;
		
		public String spacesForPrint;
		
		public Node(E value) {
			this.value = value;
		}
				
	}

	
	
	public static void main(String[] args) {
		Tree<Integer> tree = new Tree<>();
		
		System.out.println(tree.add(3));
		System.out.println(tree.add(3));
		
		System.out.println("~~~~~~~");
		tree.add(new Integer[] {1, 2, 5, 4, 6, 0});
		tree.print();
		
		System.out.println("~~~~~~~");
		System.out.println(tree.remove(3));
		System.out.println(tree.remove(3));
		tree.print();
		
	}
}
