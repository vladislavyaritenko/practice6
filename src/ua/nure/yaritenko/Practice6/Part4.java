package ua.nure.yaritenko.Practice6;


public class Part4 {
    public static void main(String[] args) {
        System.out.println("Добавляем ребра: (1;2), (1;3), (2;4), (4;5), (5;4), (3;6)");
        int vertex = 6;
        Graph graph = new Graph(vertex);
        graph.add(1, 2);
        graph.add(1, 3);
        graph.add(2, 4);
        graph.add(4, 5);
        graph.add(5, 4);
        graph.add(3, 6);
        graph.print();
        System.out.println();
        graph.remove(1,2);
        graph.print();
    }

}
