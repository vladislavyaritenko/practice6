package ua.nure.yaritenko.Practice6;


public class Part7 {
    public static void main(String[] args) {
        Range range = new Range(3, 10);
        StringBuilder sb = new StringBuilder();
        sb.append("result: ");
        for(Integer el : range){
            sb.append(el).append(" ");
        }
        sb.append(System.lineSeparator());
        //result 3 4 5 6 7 8 9 10

        range = new Range(3, 10, true);
        sb.append("result: ");
        for(Integer el : range){
            sb.append(el).append(" ");
        }
        System.out.println(sb);
        // result 10 9 8 7 6 5 4 3
    }
}
