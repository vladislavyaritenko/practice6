package ua.nure.yaritenko.Practice6;

/**
 * Created by Oleg on 07.12.2017.
 */
public class Demo {
    public static void main(String[] args) throws Exception {
        System.out.println("~~~~~~~~~~~~Part1");
        Part1.main(args);

        System.out.println("~~~~~~~~~~~~Part2");
        Part2.main(args);

        System.out.println("~~~~~~~~~~~~Part3");
        Part3.main(args);

        System.out.println("~~~~~~~~~~~~Part4");
        Part4.main(args);

       /* System.out.println("~~~~~~~~~~~~Part5");
        Part5.main(args);*/

        System.out.println("~~~~~~~~~~~~Part6");
        Part6.main(new String[]{"-i", "part6.txt", "--task", "frequency"});
        Part6.main(new String[]{"--input", "part6.txt", "--task", "length"});
        Part6.main(new String[]{"--task", "duplicates", "--input", "part6.txt"});


        /*System.out.println("~~~~~~~~~~~~Part7");
        Part7.main(args);*/
    }
}
