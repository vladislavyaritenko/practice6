package ua.nure.yaritenko.Practice6;

import java.util.Iterator;

public class Range implements Iterable <Integer> {
    private int n;
    private int m;
    private boolean reverse;
    Integer[] mas;

    public Range(int n, int m) {
        this.n = n;
        this.m = m;
        mas = init();
        iterator();
    }

    public Range(int n, int m, boolean reverse) {
        this.n = n;
        this.m = m;
        this.reverse = reverse;
        mas = init();
        iterator();
    }

    private Integer[] init() {
       Integer[] mas = new Integer[(m - n) + 1];
        for (int i = n, j = 0; i <= m; i++, j++) {
            mas[j] = i;
        }
        return mas;
    }

    public Iterator <Integer> iterator() {
        return new IteratorImpl();
    }

    private class IteratorImpl implements Iterator <Integer> {
        int nextElement;
        int lastElement;
        boolean flag;

        IteratorImpl() {
            if (reverse == false) {
                this.nextElement = 0;
                this.lastElement = -1;
            } else {
                this.nextElement = mas.length;
                this.lastElement = mas.length + 1;
            }
        }

        public boolean hasNext() {
            if (reverse == false) {
                return nextElement != mas.length;
            } else {
                return nextElement != 0;
            }

        }

        public Integer next() {
            if (reverse == false) {
                int i = nextElement;
                if (i >= mas.length) {
                    System.out.println("Элементов нет!");
                }
                nextElement = i + 1;
                lastElement = i;
                flag = true;
                return mas[lastElement];
            } else {
                int i = nextElement - 1;
                if (i < 0) {
                    System.out.println("Элементов нет!");
                }
                nextElement = i;
                lastElement = i;
                flag = true;
                return mas[lastElement];
            }
        }
    }
}
