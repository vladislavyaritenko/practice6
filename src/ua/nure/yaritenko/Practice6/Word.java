package ua.nure.yaritenko.Practice6;


public class Word {
    private String word;
    private int frequency;

    public Word(String content) {
        this.word = content;
        frequency = 1;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}