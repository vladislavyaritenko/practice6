package ua.nure.yaritenko.Practice6;

import java.io.IOException;
import java.util.*;


public class WordConteiner extends ArrayList <Word> {
    public boolean add(Word word) {
        boolean flag = false;
        for (int i = 0; i < size(); i++) {
            if (word.getWord().equals(get(i).getWord())) {
                flag = true;
                get(i).setFrequency(get(i).getFrequency() + 1);
               return true;
            }
        }
        if(!flag){
            super.add(word);
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int min = get(0).getFrequency();
        int max = get(0).getFrequency();
        for (int i = 0; i < size(); i++) {
            if (min > get(i).getFrequency()) {
                min = get(i).getFrequency();
            }
            if (max < get(i).getFrequency()) {
                max = get(i).getFrequency();
            }
        }
        for (int i = max; i > min - 1; i--) {
            ArrayList <Word> al = new ArrayList <>();
            for (int j = 0; j < size(); j++) {
                if (get(j).getFrequency() == i) {
                    al.add(get(j));
                }
            }

            Collections.sort(al, (w1, w2) -> w1.getWord().compareTo(w2.getWord()));
            for (int j = 0; j < al.size(); j++) {
                sb.append(al.get(j).getWord() + " : " + al.get(j).getFrequency()).append(System.lineSeparator());
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        WordConteiner wc = new WordConteiner();
        Scanner sc = new Scanner(System.in);
        StringBuffer sb = new StringBuffer();
        String s;
        while (sc.hasNext()) {
            s = sc.next();
            if ("stop".equals(s)) {
                break;
            }
            sb.append(s).append(" ");
        }
        sc.close();
        String[] strings = sb.toString().split("\\s+");
        for (int i = 0; i < strings.length; i++) {
            wc.add(new Word(strings[i]));
        }

        System.out.println(wc.toString());

    }


}