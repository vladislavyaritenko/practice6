package ua.nure.yaritenko.Practice6;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;

public class Part3 {
    public static void main(String[] args) {
        Parking parking = new Parking(5);
        System.out.println("Car 1 arrive at 2d place");
        parking.arrive(2, "Car 1");
        System.out.println(parking.place);
        System.out.println();
        System.out.println("Car 2 arrive at 2d place");
        parking.arrive(2, "Car 2");
        System.out.println(parking.place);
        System.out.println();
        System.out.println("Car 3 arrive at 2th place");
        parking.arrive(2, "Car 3");
        System.out.println(parking.place);
        System.out.println();
        System.out.println("Car 4 arrive at 5th place");
        parking.arrive(5, "Car 4");
        System.out.println(parking.place);
        System.out.println();
        System.out.println("Car 4 leave at 5th place");
        parking.leave(5, "Car 4");
        System.out.println(parking.place);
        System.out.println();

    }
}
