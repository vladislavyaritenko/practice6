package ua.nure.yaritenko.Practice6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Graph {
    private int vertex; //кол-во вершин
    int[][] matrix;

    public Graph(int vertex) {
        this.vertex = vertex;
        matrix = new int[this.vertex][this.vertex];
    }

    public void print() {
        System.out.print("    ");
        for(int i = 0; i < matrix.length; i++){
            System.out.print(i+1 + ") ");
        }
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(i + 1 + ")");
            for (int j = 0; j < matrix.length; j++) {
                System.out.print("  " + matrix[i][j]);
            }
            System.out.println();
        }
    }

    public void add(int firstVertex, int secondVertex) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (((i+1 == firstVertex) & (j+1 == secondVertex))) {
                    matrix[i][j] = 1;
                }
                else if(matrix[i][j] == 1){
                    matrix[j][i] = 0;;
                }

            }
        }
    }

    public void remove(int firstVertex, int secondVertex) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (((i+1 == firstVertex) & (j+1 == secondVertex))) {
                    matrix[i][j] = 0;
                }
                /*else if(matrix[i][j] == 1){
                    matrix[j][i] = 0;;
                }*/

            }
        }
    }
}
