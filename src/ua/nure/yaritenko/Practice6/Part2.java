package ua.nure.yaritenko.Practice6;

import java.util.*;

public class Part2 {

    private static void init(List <Integer> list) {
        for (int i = 0; i < 20000; i++) {
            list.add(i + 1);
        }
    }

    private static void removeIter(List <Integer> list, int k) {
        int count = k;
        int kk = 0;
        while (list.size() > 1) {
            Iterator <Integer> iter = list.iterator();
            while (iter.hasNext()) {
                iter.next();
                if (kk == count - 1) {
                    iter.remove();
                    count += k - 1;
                }
                kk++;
            }
        }
    }

    private static void removeIndex(List <Integer> list, int k) {
        int count = k;
        while (list.size() > 1) {
            if (count > list.size()) {
                count -= list.size();
            } else {
                list.remove(count - 1);
                count += k - 1;
            }
        }
    }

    public static void main(String[] args) {
        ArrayList <Integer> al = new ArrayList <>();
        init(al);
        ArrayList <Integer> al2 = new ArrayList <>();
        init(al2);
        LinkedList <Integer> ll = new LinkedList <>();
        init(ll);
        LinkedList <Integer> ll2 = new LinkedList <>();
        init(ll2);
        long before;
        long after;
        long resultTime;
        before = System.currentTimeMillis();
        removeIter(al, 4);
        after = System.currentTimeMillis();
        resultTime = after - before;
        System.out.println("ArrayList#removeIter#time ==> " + resultTime + " ms.");
        before = System.currentTimeMillis();
        removeIndex(al2, 4);
        after = System.currentTimeMillis();
        resultTime = after - before;
        System.out.println("ArrayList#removeIndex#time ==> " + resultTime + " ms.");
        before = System.currentTimeMillis();
        removeIter(ll, 4);
        after = System.currentTimeMillis();
        resultTime = after - before;
        System.out.println("LinkedList#removeIter#time ==> " + resultTime + " ms.");
        before = System.currentTimeMillis();
        removeIndex(ll2, 4);
        after = System.currentTimeMillis();
        resultTime = after - before;
        System.out.println("LinkedList#removeIndex#time ==> " + resultTime + " ms.");
    }
}
