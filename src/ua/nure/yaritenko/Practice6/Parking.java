package ua.nure.yaritenko.Practice6;

import java.util.ArrayList;

public class Parking {
    ArrayList <String> place;

    public Parking(int n) {
        place = new ArrayList <>();
        for (int i = 0; i < n; i++) {
            place.add("free place");
        }
    }

    public void arrive(int index, String nameCar) {
        for (int i = index - 1; i < place.size(); i++) {
            if (place.get(i).equals("free place")) {
                place.set(i, nameCar);
                break;
            }
        }
    }

    public void leave(int index, String nameCar) {
        for (int i = index - 1; i < place.size(); i++) {
            if (place.get(i).equals(nameCar)) {
                place.set(i, "free place");
                break;
            }
        }
    }
}
