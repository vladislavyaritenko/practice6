package ua.nure.yaritenko.Practice6;

import java.util.*;

class Wordd {
    private String word;
    private int frequency;
    private int length;

    public Wordd(String content) {
        this.word = content;
        frequency = 1;
        length = 0;
    }

    public String getWord() {
        return word;
    }

    public int getLength() {
        return length;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}


class Frequency extends ArrayList <Wordd> {
    String[] strings;

    Frequency(String[] s) {
        strings = s;
    }

    public boolean add(Wordd word) {
        boolean flag = false;
        for (int i = 0; i < size(); i++) {
            if (word.getWord().equals(get(i).getWord())) {
                flag = true;
                get(i).setFrequency(get(i).getFrequency() + 1);
                return true;
            }
        }
        if (!flag) {
            super.add(word);
            return true;
        }
        return false;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        int min = get(0).getFrequency();
        int max = get(0).getFrequency();
        for (int i = 0; i < size(); i++) {
            if (min > get(i).getFrequency()) {
                min = get(i).getFrequency();
            }
            if (max < get(i).getFrequency()) {
                max = get(i).getFrequency();
            }
        }
        int cur = 0;
        ArrayList <Wordd> al = new ArrayList <>();
        for (int i = max; i > min - 1; i--) {
            for (int j = 0; j < size(); j++) {
                if (get(j).getFrequency() == i) {
                    if (cur == 3) {
                        break;
                    } else if ((cur < 3)) {
                        al.add(get(j));
                        cur++;
                    }

                }

            }
        }
        al.sort((w1, w2) -> (w2.getWord().compareToIgnoreCase(w1.getWord())));
        for (int k = 0; k < 3; k++) {
            sb.append(al.get(k).getWord() + " ==> " + al.get(k).getFrequency()).append(System.lineSeparator());

        }
        return sb.toString();
    }
}


class Length extends ArrayList <Wordd> {
    String[] strings;

    Length(String[] s) {
        strings = s;
    }

    public boolean add(Wordd word) {
        boolean flag = false;
        for (int i = 0; i < size(); i++) {
            if (word.getWord().equals(get(i).getWord())) {
                return false;
            }
        }
        if (!flag) {
            word.setLength(word.getWord().length());
            super.add(word);
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        ArrayList <Wordd> al = new ArrayList <>();
        int maxLength = 0;
        for (Wordd wordd : this) {
            if (wordd.getLength() > maxLength) {
                maxLength = wordd.getLength();
            }
        }
        for (int i = maxLength; i > 0; i--) {
            for (int j = 0; j < size(); j++) {
                if (get(j).getLength() == i) {
                    al.add(get(j));
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            sb.append(al.get(i).getWord() + " ==> " + al.get(i).getLength()).append(System.lineSeparator());
        }
        return sb.toString();
    }
}


class Duplicates extends ArrayList <Wordd> {
    String[] strings;

    Duplicates(String[] s) {
        strings = s;
    }

    public boolean add(Wordd word) {
        boolean flag = false;
        for (int i = 0; i < size(); i++) {
            if (word.getWord().equals(get(i).getWord())) {
                flag = true;
                get(i).setFrequency(get(i).getFrequency() + 1);
                return true;
            }
        }
        if (!flag) {
            super.add(word);
            return true;
        }
        return false;
    }


    public void print() {
        for (int i = 0; i < size(); i++) {
            System.out.println(get(i).getWord());
        }
    }

    public ArrayList <Wordd> duplicate() {
        ArrayList <Wordd> al = new ArrayList <>();
        for (int i = 0; i < size(); i++) {
            if (get(i).getFrequency() > 1) {
                al.add(get(i));
            }
        }
        return al;
    }

    public String toString() {
        ArrayList <Wordd> al = duplicate();
        StringBuilder sb = new StringBuilder();
        String[] strings = new String[3];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = al.get(i).getWord().toUpperCase();
        }
        for (int i = 0; i < strings.length; i++) {
            sb.append(strings[i]).reverse();
            strings[i] = sb.toString();
            sb.delete(0, strings[i].length());
        }


        for (int i = 0; i < 3; i++) {
            sb.append(strings[i]).append(System.lineSeparator());
        }
        return sb.toString();
    }
}


public class Part6 {
    public static void main(String[] args) {
        switch (args[0]) {
            case "-i":
            case "--input": {
                if (args[1].equalsIgnoreCase("part6.txt")) {
                    String input = Util.getInput("part6.txt");
                    String[] strings = input.split("\\s+");
                    switch (args[2]) {
                        case "-t":
                        case "--task": {
                            if (args[3].equalsIgnoreCase("frequency")) {
                                Frequency wc = new Frequency(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    wc.add(new Wordd(strings[i]));
                                }
                                System.out.println(wc.toString());
                            }
                            else if (args[3].equalsIgnoreCase("length")) {
                                Length length = new Length(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    length.add(new Wordd(strings[i]));
                                }
                                System.out.println(length.toString());

                            }
                            else if (args[3].equalsIgnoreCase("duplicates")) {
                                Duplicates duplicate = new Duplicates(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    duplicate.add(new Wordd(strings[i]));
                                }
                                System.out.println(duplicate.toString());
                            }
                            break;
                        }
                    }
                }
                break;
            }
            case "-t":
            case "--task": {
                switch (args[2]) {
                    case "-i":
                    case "--input": {
                        if (args[3].equalsIgnoreCase("part6.txt")) {
                            String input = Util.getInput("part6.txt");
                            String[] strings = input.split("\\s+");
                            if (args[1].equalsIgnoreCase("frequency")) {
                                Frequency wc = new Frequency(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    wc.add(new Wordd(strings[i]));
                                }
                                System.out.println(wc.toString());
                            }
                            else if (args[1].equalsIgnoreCase("length")) {
                                Length length = new Length(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    length.add(new Wordd(strings[i]));
                                }
                                System.out.println(length.toString());
                            }
                            else if (args[1].equalsIgnoreCase("duplicates")) {
                                Duplicates duplicate = new Duplicates(strings);
                                for (int i = 0; i < strings.length; i++) {
                                    duplicate.add(new Wordd(strings[i]));
                                }
                                System.out.println(duplicate.toString());
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }

        /*String input = Util.getInput("part6.txt");
        String[] strings = input.split("\\s+");*/

        //frequency
       /* Frequency wc = new Frequency(strings);
        for (int i = 0; i < strings.length; i++) {
            wc.add(new Wordd(strings[i]));
        }
        System.out.println(wc.toString());*/
        //length
        /*Length length = new Length(strings);
        for (int i = 0; i < strings.length; i++) {
            length.add(new Wordd(strings[i]));
        }
        System.out.println(length.toString());*/
        //duplicates
       /* Duplicates duplicate = new Duplicates(strings);
        for (int i = 0; i < strings.length; i++) {
            duplicate.add(new Wordd(strings[i]));
        }
        System.out.println(duplicate.toString());*/

    }
}
